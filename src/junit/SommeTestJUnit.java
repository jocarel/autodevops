package junit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import object.MyObject;

class SommeTestJUnit {
	
	private MyObject obj = new MyObject(5, 2);

	@Test
	void addTest() {
		double db = obj.add(2);
		assertTrue(db == 9);
	}
	
	@Test
	void substractTest() {
		double db = obj.substract(2);
		assertTrue(db == 5);
	}
}
